import React, { useState, useEffect } from 'react';
import GridLayout from 'react-grid-layout';
import 'react-grid-layout/css/styles.css';
import 'react-resizable/css/styles.css';
import axios from 'axios';

const App = () => {
  const [layout, setLayout] = useState([]);

  useEffect(() => {
    axios.get('/api/layout').then(response => {
      setLayout(response.data);
    });
  }, []);

  const saveLayout = (layout) => {
    axios.post('/api/layout', layout).then(response => {
      console.log(response.data);
    });
  };

  const addWidget = () => {
    const newWidget = {
      i: `widget-${layout.length + 1}`,
      x: (layout.length * 2) % 12,
      y: Infinity,
      w: 2,
      h: 2
    };
    setLayout([...layout, newWidget]);
  };

  return (
    <div>
      <button onClick={addWidget}>Add Widget</button>
      <button onClick={() => saveLayout(layout)}>Save Layout</button>
      <GridLayout
        className="layout"
        layout={layout}
        cols={12}
        rowHeight={30}
        width={1200}
        onLayoutChange={(layout) => setLayout(layout)}
      >
        {layout.map(item => (
          <div key={item.i} style={{ border: '1px solid #ccc', backgroundColor: '#fafafa' }}>
            {item.i}
          </div>
        ))}
      </GridLayout>
    </div>
  );
};

export default App;
