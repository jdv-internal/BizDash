const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const cors = require('cors');

app.use(bodyParser.json());
app.use(cors());

let layout = [];

app.get('/api/layout', (req, res) => {
  res.json(layout);
});

app.post('/api/layout', (req, res) => {
  layout = req.body;
  res.json({ status: 'Layout saved' });
});

app.listen(3001, () => console.log('Server running on http://localhost:3001'));
